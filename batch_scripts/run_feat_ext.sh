# This job is a single process that needs 8gb of physical memory and 9gb of virtual memory
# (it can push 1GB into swap/paging space.)
#PBS -l nodes=1:ppn=1,pmem=8gb,pvmem=10gb
# It should be allowed to run for up to 1 hour.
#PBS -l walltime=04:00:00
# Name of job.
#PBS -N mybigmemjob
# Join STDERR TO STDOUT.  (omit this if you want separate STDOUT AND STDERR)
#PBS -j oe   
cd $HOME/deep_learning
source activate py36dg
time python feature_extractor.py
