#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb  3 19:09:21 2020

@author: max
"""

import numpy as np
from keras.applications import vgg16
from keras import Model, Sequential
from keras.layers import Dense
from sklearn.svm import SVC
from keras.regularizers import L1L2
from keras.utils import to_categorical
import json
from keras.preprocessing.image import ImageDataGenerator
from keras.utils import np_utils 
import glob
def extract_features():
    
    num_classes = len(glob.glob('data/output/train/*'))
    #Load the VGG model
    vgg_model = vgg16.VGG16(weights='imagenet')
     
    vgg_model.summary()
    
    # make new model with output of the final fully connected layer
    
    model_output = vgg_model.get_layer("fc2").output
    m = Model(inputs=vgg_model.input, outputs=model_output)
    # remove final layer
    
    
    # use generator class to extract features by passing them through the neural network
        
    data_gen = ImageDataGenerator()
    
    train_generator = data_gen.flow_from_directory(
        'data/output/train',
        target_size=(224, 224),
        batch_size=32,
        class_mode='categorical',
        shuffle = False)
    
    
    X_train = m.predict(train_generator)

    y_train = train_generator.classes
    y_train = np_utils.to_categorical(y_train, num_classes)
    
    val_generator = data_gen.flow_from_directory(
        'data/output/val',
        target_size=(224, 224),
        batch_size=32,
        class_mode='categorical',
        shuffle = False)

    X_val = m.predict(val_generator)
    
    y_val = val_generator.classes
    y_val = np_utils.to_categorical(y_val, num_classes)

     
    #Set up the logistic regression model
    
    output_dim = num_classes 
    input_dim = 4096
    
    model = Sequential()
    model.add(Dense(output_dim, input_dim=input_dim, activation='softmax')) 
    model.compile(optimizer='sgd', loss='categorical_crossentropy', metrics=['accuracy']) 
    hist = model.fit(X_train, y_train, epochs=50, validation_data=(X_val, y_val))
    hist_dict = hist.history
    
    json.dump(str(hist_dict),open('history_feature_ext.json','w'))

extract_features()


