from keras.models import Sequential
from keras import optimizers
from keras.layers import Dense, Activation, Dropout, Flatten
from keras.layers import Conv2D
from keras.layers import MaxPooling2D
import numpy as np
from keras.preprocessing.image import ImageDataGenerator
# load data into workspace
import json
input_shape = (224, 224, 3)

def main():
#Instantiate an empty model
    model = Sequential([
    Conv2D(64, (3, 3), input_shape=input_shape, padding='same', activation='relu'),
    Conv2D(64, (3, 3), activation='relu', padding='same'),
    MaxPooling2D(pool_size=(2, 2), strides=(2, 2)),
    Conv2D(128, (3, 3), activation='relu', padding='same'),
    Conv2D(128, (3, 3), activation='relu', padding='same',),
    MaxPooling2D(pool_size=(2, 2), strides=(2, 2)),
    Conv2D(256, (3, 3), activation='relu', padding='same',),
    Conv2D(256, (3, 3), activation='relu', padding='same',),
    Conv2D(256, (3, 3), activation='relu', padding='same',),
    MaxPooling2D(pool_size=(2, 2), strides=(2, 2)),
    Conv2D(512, (3, 3), activation='relu', padding='same',),
    Conv2D(512, (3, 3), activation='relu', padding='same',),
    Conv2D(512, (3, 3), activation='relu', padding='same',),
    MaxPooling2D(pool_size=(2, 2), strides=(2, 2)),
    Conv2D(512, (3, 3), activation='relu', padding='same',),
    Conv2D(512, (3, 3), activation='relu', padding='same',),
    Conv2D(512, (3, 3), activation='relu', padding='same',),
    MaxPooling2D(pool_size=(2, 2), strides=(2, 2)),
    Flatten(),
    Dense(4096, activation='relu'),
    Dense(4096, activation='relu'),
    Dense(250, activation='softmax')
    ])
    
    sgd = optimizers.SGD(lr=0.00001, decay=1e-6, momentum=0.9, nesterov=True)
    
    model.compile(optimizer=sgd,
                  loss='categorical_crossentropy',
                  metrics=['accuracy'])
    
    train_datagen = ImageDataGenerator()
    
    test_datagen = ImageDataGenerator()
    
    train_generator = train_datagen.flow_from_directory(
            'data/output/train',
            target_size=(224, 224),
            batch_size=32,
            class_mode='categorical')
    
    validation_generator = test_datagen.flow_from_directory(
            'data/output/val',
            target_size=(224, 224),
            batch_size=32,
            class_mode='categorical')
    
    hist = model.fit_generator(
            train_generator,
            steps_per_epoch=500,
            epochs=2,
            validation_data=validation_generator,
            validation_steps=125)

    history  = hist.history
    json.dump(history,open('history_scratch_model.json','w'))

main()
