#!/bin/bash
# specify a partition
#SBATCH --partition=dggpu
# Request nodes
#SBATCH --nodes=1
# Request some processor cores
#SBATCH --ntasks=32
# Request GPUs
#SBATCH --gres=gpu:4
# Request memory 
#SBATCH --mem=16G
# Maximum runtime of 10 minutes
#SBATCH --time=10:00:00
# Name of this job
#SBATCH --job-name=fine_tune
# Output of this job, stderr and stdout are joined by default
# %x=job-name %j=jobid
#SBATCH --output=..%x_%j.out
# change to the directory where you submitted this script
cd ${SLURM_SUBMIT_DIR}
# your job execution follows:
source activate py36dg
time python fine_tune.py

