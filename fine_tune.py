
from keras.models import Sequential
from keras.optimizers import SGD
from keras.layers import Input, Dense, Convolution2D, MaxPooling2D, AveragePooling2D, ZeroPadding2D, Dropout, Flatten, merge, Reshape, Activation
import numpy as np
from sklearn.metrics import log_loss
from keras.applications import vgg16
from keras.preprocessing.image import ImageDataGenerator
import json 
import glob


def vgg16_model():
    """VGG 16 Model for Keras
    Parameters:
      img_rows, img_cols - resolution of inputs
      channel - 1 for grayscale, 3 for color 
      num_classes - number of categories for our classification task
      
      
      
    """
    
    input_shape = (224, 224, 3)
    
    model = Sequential()
    model.add(ZeroPadding2D((1, 1), input_shape=input_shape))
    model.add(Convolution2D(64, 3, 3, activation='relu'))
    model.add(ZeroPadding2D((1, 1)))
    model.add(Convolution2D(64, 3, 3, activation='relu'))
    model.add(MaxPooling2D((2, 2), strides=(2, 2)))

    model.add(ZeroPadding2D((1, 1)))
    model.add(Convolution2D(128, 3, 3, activation='relu'))
    model.add(ZeroPadding2D((1, 1)))
    model.add(Convolution2D(128, 3, 3, activation='relu'))
    model.add(MaxPooling2D((2, 2), strides=(2, 2)))

    model.add(ZeroPadding2D((1, 1)))
    model.add(Convolution2D(256, 3, 3, activation='relu'))
    model.add(ZeroPadding2D((1, 1)))
    model.add(Convolution2D(256, 3, 3, activation='relu'))
    model.add(ZeroPadding2D((1, 1)))
    model.add(Convolution2D(256, 3, 3, activation='relu'))
    model.add(MaxPooling2D((2, 2), strides=(2, 2)))

    model.add(ZeroPadding2D((1, 1)))
    model.add(Convolution2D(512, 3, 3, activation='relu'))
    model.add(ZeroPadding2D((1, 1)))
    model.add(Convolution2D(512, 3, 3, activation='relu'))
    model.add(ZeroPadding2D((1, 1)))
    model.add(Convolution2D(512, 3, 3, activation='relu'))
    model.add(MaxPooling2D((2, 2), strides=(2, 2)))

    model.add(ZeroPadding2D((1, 1)))
    model.add(Convolution2D(512, 3, 3, activation='relu'))
    model.add(ZeroPadding2D((1, 1)))
    model.add(Convolution2D(512, 3, 3, activation='relu'))
    model.add(ZeroPadding2D((1, 1)))
    model.add(Convolution2D(512, 3, 3, activation='relu'))
    model.add(MaxPooling2D((2, 2), strides=(2, 2)))

    # Add Fully Connected Layer
    model.add(Flatten())
    model.add(Dense(4096, activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(4096, activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(1000, activation='softmax'))
    # Loads ImageNet pre-trained data
    model.load_weights('vgg16_weights.h5',by_name=True)
    
    num_class = len(glob.glob('data/output/train/*'))

    
    
    # replace softmax layer for transfer learning
    model.layers.pop()
    model.outputs = [model.layers[-1].output]
    model.layers[-1].outbound_nodes = []
    model.add(Dense(num_class, activation='softmax'))
    
    
    # freeze the first ten layers 
    for layer in model.layers[:5]:
        layer.trainable = False
    
    
    sgd = SGD(lr=0.00001, decay=1e-6, momentum=0.9, nesterov=True)
    model.compile(optimizer=sgd, loss='categorical_crossentropy', metrics=['accuracy'])

    model.summary()
    return model


if __name__ == '__main__':


    # Load our modl
    model = vgg16_model()

    # Start Fine-tuning
    
    train_datagen = ImageDataGenerator()
    
    test_datagen = ImageDataGenerator()
    
    train_generator = train_datagen.flow_from_directory(
            'data/output/train',
            target_size=(224, 224),
            batch_size=32,
            class_mode='categorical')
    
    validation_generator = test_datagen.flow_from_directory(
            'data/output/val',
            target_size=(224, 224),
            batch_size=32,
            class_mode='categorical')
    
    hist = model.fit_generator(
            train_generator,
            steps_per_epoch=500,
            epochs=10,
            validation_data=validation_generator,
            validation_steps=125)
        
    history  = hist.history
    json.dump(str(history),open('history_fine_tune.json','w'))

