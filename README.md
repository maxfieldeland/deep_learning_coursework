# deep_learning_coursework

Course work for the UVM Deep Learning 2020 Course!

Includes two homework assignments focused on solving deep learning tasks and running computation on DeepGreen, the remote GPU cluster at UVM. 