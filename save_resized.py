#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Feb  9 14:24:59 2020

@author: max
"""

import glob
import numpy as np
import imageio
from skimage.transform import resize
import os

def main():
    '''
    open, resize and resave all images as pngs in png_resize\ subdirectory
    
    '''
    categories = sorted(glob.glob('data/png/*'))    
    for category in categories[:4]:
        # make new subfolder
        folder_name = category.split('/')[2]
        # check to see if folder exists, if not then make new directory
        try:
            os.mkdir('data/png_resize/' +folder_name)
        except:
            FileExistsError
        # loop through the subcategories 
        for test_filename in glob.glob(category+'/*'):
            image = np.array(imageio.imread(test_filename))
            # resize image
            image = resize(image, (224,224,3))
            number = test_filename.split('/')[3]
            number = number.split('.')[0]
            file_path = 'data/png_resize/' + folder_name + '/'+ number + '.png'
            # save image
            imageio.imwrite(file_path, image)
            
            
main()